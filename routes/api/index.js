var express = require('express');
var router = express.Router();
const dateformat = require('dateformat');

today = new Date();

/* GET to show api. */
router.get('/', function(req, res, next) {
    console.log("Got to / ");
    console.log( dateformat(today, 'mm/dd/yyyy'));
    res.status(200).json({ message: "Welcome to bolao api"})
});

module.exports = router;
