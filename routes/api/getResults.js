var parser = require('rss-parser');
var express = require('express');
var router = express.Router();

var reg = new RegExp(/<br\s*\/?>/g);
var leagueList = {"root":[]};//{['name'='', 'location'= '','matches'=[{'team1Name': '', 'team1Score':'','team2Name': '', 'team2Score':'',}],]};

var league = "none";
var location = "none";
var matchList = []; // [{'team1Name': '', 'team1Score':'','team2Name': '', 'team2Score':''},]

team1Name="";
team2Name="";
team1Score="";
team2Score="";
dashPos = -1;


// middleware to use for all requests - add authentication
router.use(function(req, res, next) {
    // do logging
    console.log('Someone is accessing the results');
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res, next) {
    parser.parseURL('http://www.soccerstats247.com/DailyMatchFeed.aspx?langId=12', function(err, parsed) {
      //console.log(parsed.feed.title);
      parsed.feed.entries.forEach(function(entry) {
        //console.log(entry.title + ':' + entry.link);
        //console.log(entry.content.split(reg));
        if (entry.title.indexOf('05/20/2017')>0){
            //console.log(entry.title + ':' + entry.link);
            //console.log(entry.content.split(reg));
            values = entry.content.split(reg);
            values.forEach(function(value){
                if (value.match('<b>')){
                    value = value.replace('<b>',"");
                    value = value.replace('</b>',"");
                    value = value.replace('</center>',"");
                    if (league != "none"){ // All other readings on league
                        leagueList.root.push({
                            'name':league ,
                            'location': location ,
                            'matches':matchList ,
                        });

                        // clear variables
                        league = "none";
                        location = "none";
                        matchList = [];

                    }

                    location = value.split(': ')[0]
                    league = value.split(': ')[1];
                }
                else if((value.length<50)&&(value.length>9)){ // Reading on matches
                    value = value.replace('<b>',"");
                    value = value.replace('</b>',"");
                    value = value.replace('</center>',"");
                    value = value.replace('<center>',"");

                    dashPos = value.indexOf(' - ')

                    team1Name= value.slice(0,dashPos-2);
                    team2Name= value.slice(dashPos+5,value.length);

                    team1Score= value.slice(dashPos-2,dashPos);
                    team2Score= value.slice(dashPos+3,dashPos+4);

                    //console.log(team1Name + " " + team1Score + " : " + team2Score + " " + team2Name);

                    matchList.push({
                        'team1Name':team1Name ,
                        'team1Score': team1Score ,
                        'team2Name':team2Name ,
                        'team2Score':team2Score ,
                        });
                }
            })
        }
        //console.log(Object.getOwnPropertyNames(entry));
      })
      //console.log(leagueList);
      res.status(200).json(leagueList);
    })

    console.log('Done');
});

module.exports = router;
