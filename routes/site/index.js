var express = require('express');
var router = express.Router();

/* GET to show site index. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Hey', message: 'Hello there! This is the index page!',tab: 'index'});
});


module.exports = router;
