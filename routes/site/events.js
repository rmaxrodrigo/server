var express = require('express')
,   mongoose = require('mongoose')
,   EventModel = require('../../models/events')
//,   MatchModel = require('../../models/matches')
,   router = express.Router()
,   log4js = require("log4js");

var logger = log4js.getLogger();


mongoose.Promise = require('bluebird');

/* GET to show site index. */
router.get('/', function(req, res, next) {

    EventModel.find({}, function (err, eventsList) {
        if (err){
            console.log(err)
            return
        }
        // 'events' contains the list of events from db
        //console.log(eventsList)

        logger.warn("TESTE")

        res.render('events', { title: 'Events available', message: 'Choose one event to view the matches',tab: 'events', 'events': eventsList})

    })



})


module.exports = router
