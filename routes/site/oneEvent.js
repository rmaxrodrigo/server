var express = require('express')
,   router = express.Router()
,   mongoose = require('mongoose')
,   EventModel = require('../../models/events')
,   MatchModel = require('../../models/matches')
,   TeamModel = require('../../models/teams')
,   auxFunctions = require('../../custom_modules/functions/site')
,   ObjectID = require('mongodb').ObjectID
,   Schema = mongoose.Schema
,   router = express.Router();

mongoose.Promise = require('bluebird');

/* GET to show site index. */
router.get('/', function(req, res, next) {

    auxFunctions["functionName"]("Mensagem bem sucedida!")

    EventModel.findOne({"urlID": res.params.urlID})
        .exec(function (err, oneEventInfo) {
            if (err){
                console.log(err)
                return
            }
            matchesResult = []
            MatchModel.find({"eventID": oneEventInfo._id})
                .populate('homeTeamID')
                .populate('awayTeamID')
                .exec(function (err, matchesResult) {
                    if (err){
                        console.log(err)
                        return
                    }
                    res.render('oneEvent', { title: 'Hey!', message: 'Description of the event!', info: oneEventInfo, matches: matchesResult});
            })
        })

})


module.exports = router;
