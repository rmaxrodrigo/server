var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MatchSchema = new Schema({
    eventID                 : { type: Schema.ObjectId, ref: 'Event' },
    matchLocationVenue      : String,
    homeTeamID              : { type: Schema.ObjectId, ref: 'Team' },
    matchDate               : String,
    awayTeamID              : { type: Schema.ObjectId, ref: 'Team' },
    awayScore               : Number,
    homeScore               : Number,
    phase                   : String,
    phaseInfo               : String,
    matchLocationStadium    : String

},{ collection: 'Match' })

module.exports = mongoose.model('Match', MatchSchema )
