var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EventSchema = new Schema({
    urlID       : String,
    name        : String,
    season      : Number,
    description : String,
    matches     : [{ type: Schema.ObjectId, ref: 'Match' }]

},{ collection: 'Event' })

module.exports = mongoose.model('Event', EventSchema )
