var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RuleSchema = new Schema({
    name            :   String,
    description     :   String,
    functionName    :   String,
    category        :   String // gameplay,score,timing

},{ collection: 'Rule' })

module.exports = mongoose.model('Rule', RuleSchema )



/*        NotifyOnMatchUpdate                         : Boolean,
        Score                                       : {
            ExactScore                              : Number,
            TotalGoals                              : Number,
            WinnerScoreRightWrongWinner             : Number,
            LoserScoreRightWrongWinner              : Number,
            WinnerScoreRightWrongWinnerWithGoals    : Number,
            LoserScoreRightWrongWinnerWithGoals     : Number,
            WinnerAndWinnerScore                    : Number,
            WinnerAndLoserScore                     : Number,
            WinnerAndWinnerScoreWithGoals           : Number,
            WinnerAndLoserScoreWithGoals            : Number,
        }
    LimitToPlaceGuess                               :{
            Active                                  : Boolean,
            HoursBeforeMatch                        : Date
        }*/
