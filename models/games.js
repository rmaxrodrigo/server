var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var gameSchema = new Schema({
    eventID                                         : { type: Schema.ObjectId, ref: 'Event' },,
    ownerID                                         : { type: Schema.ObjectId, ref: 'User' },,
    participantsIDs                                 : [{ type: Schema.ObjectId, ref: 'User' },],
    status                                          : String, // Planned, Ongoing, Ended
    rules                                           : [
        rule                                        : {
            ruleID                                  : { type: Schema.ObjectId, ref: 'Rule' },,
            ruleStringPoints                        : String,
            ruleBoolPoints                          : Boolean,
            ruleNumberPoints                        : Number
        }
    ]

},{ collection: 'Game' })

module.exports = mongoose.model('Game', gameSchema )
