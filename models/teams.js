var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TeamSchema = new Schema({
    country         : String,
    name            : String,
    logo            : String,
    matches         : [{ type: Schema.ObjectId, ref: 'Match' }]

},{ collection: 'Team' })

module.exports = mongoose.model('Team', TeamSchema )
