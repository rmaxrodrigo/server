docker run \
--name=splash_01  \
-h splashDocker \
--restart=unless-stopped \
-d -p 8050:8050 \
-p 8051:8051 \
-e "TZ=America/Sao_Paulo" \
scrapinghub/splash -v3
