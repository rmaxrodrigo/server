# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

from scrapy.conf import settings

class BolaoscrapPipeline(object):
    def process_item(self, item, spider):
        return item



class MongoDBPipelineMatches(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.matchCollection = db[settings['MONGODB_MATCH_COLLECTION']]
        self.eventCollection = db[settings['MONGODB_EVENT_COLLECTION']]
        self.teamCollection = db[settings['MONGODB_TEAM_COLLECTION']]

        self.currentEventID = self.eventCollection.find_one({ \
        'name':settings['CURRENT_EVENT']},{'_id':1})

    #Use Regex to find team ID on database
    def deepNameSearch(self,teamName):
        if (' ' in teamName != -1):
            splittedName = teamName.split(' ')
            if(len(splittedName)>2):
                #print("Team found")
                return self.teamCollection.find_one({'name': {'$regex': splittedName[1] } },{'_id':1})
            else:
                reg = '(' + splittedName[0] + ')(.*)(' + splittedName[1] + ')'
                #print("Team found with REGEX")
                return self.teamCollection.find_one({'name': {'$regex': reg } },{'_id':1})


    #Processes items to add matches on database
    def process_item(self, item, spider):
        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:

            teamID = self.teamCollection.find_one({'name': {'$regex': item['homeTeamID']}},{'_id':1})

            if teamID is None:
                teamID = self.deepNameSearch(item['homeTeamID'])

            item['homeTeamID'] = teamID['_id']

            teamID = self.teamCollection.find_one({'name': {'$regex': item['awayTeamID']}},{'_id':1})

            if teamID is None:
                teamID = self.deepNameSearch(item['awayTeamID'])

            item['awayTeamID'] = teamID['_id']

            item['EventID'] = self.currentEventID['_id']

            resp = self.matchCollection.update({'matchDate': item['matchDate'], \
            'homeTeamID': item['homeTeamID'],'awayTeamID': item['awayTeamID']}, \
            dict(item), upsert=True)


            #Remove same entries to avoid duplicates
            self.teamCollection.update({'_id': item['homeTeamID']}, {'$pull':{ \
            'matches': {'id': resp['upserted']} }})
            self.teamCollection.update({'_id': item['awayTeamID']}, {'$pull':{ \
            'matches': {'id': resp['upserted']} }})
            self.teamCollection.update({'_id': item['homeTeamID']}, {'$push':{ \
            'matches': {'id': resp['upserted']} }}, upsert=True)
            self.teamCollection.update({'_id': item['awayTeamID']}, {'$push':{ \
            'matches': {'id': resp['upserted']} }}, upsert=True)

            #log.msg("Match added to database!",
            #        level=log.DEBUG, spider=spider)
        return item

class MongoDBPipelineTeams(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.matchCollection = db[settings['MONGODB_TEAM_COLLECTION']]

    def process_item(self, item, spider):
        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:
            #self.collection.insert(dict(item))

            self.matchCollection.update({'name': item['name'], \
            'country': item['country']}, dict(item), upsert=True)
            #log.msg("Match added to database!",
            #        level=log.DEBUG, spider=spider)
        return item
