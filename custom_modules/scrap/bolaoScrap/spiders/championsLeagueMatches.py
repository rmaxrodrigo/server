# -*- coding: utf-8 -*-
import scrapy
import json
import datetime
from scrapy_splash import SplashRequest

script = """
function main(splash)
	splash:go(splash.args.url)
    splash:wait(2)
    return {html=splash:html()}
end
"""

class championsLeagueMatchesSpider(scrapy.Spider):
	name = 'championsLeagueMatches'
	allowed_domains = ['http://www.uefa.com/']
	start_urls = ['http://www.uefa.com/uefachampionsleague/season=2017/matches/index.html']
	custom_settings = {
		'ITEM_PIPELINES': {
			'bolaoScrap.pipelines.MongoDBPipelineMatches': 300
			}
		}

	# Group Stage - MatchDay 1 - 5
	# Round Of 16 - 1ST/2ND LEG
	# Quarter-Finals - 1ST/2ND LEG
	# Semi-Finals - 1ST/2ND LEG
	# Final
	pages2Visit = ["Group stage - Matchday 1", "Group stage - Matchday 2", \
		"Group stage - Matchday 3", "Group stage - Matchday 4", \
		"Group stage - Matchday 5", "Group stage - Matchday 5", \
		"Round of 16 - 1st leg", "Round of 16 - 2nd leg", \
		"Quarter-finals - 1st leg", "Quarter-finals - 2nd leg", \
		"Semi-finals - 1st leg", "Semi-finals - 2nd leg", "Final"]

	def start_requests(self):
		for url in self.start_urls:
			yield SplashRequest(url, self.parse, args={'wait': 5, 'images_enabled' : False})

	def parse(self, response):

		pageSelector = response.xpath('//div[@class="owl-stage-outer"\
		]/div[@class="owl-stage"]')

		currentPage = pageSelector.xpath('div[contains(concat(" ", 	@class, \
		" "), " owl-item ")]/div[contains(concat(" ", 	@class, " "), \
		" item active ")]/a/text()').extract_first()

		#currentPage = " "

		currentPage = ' '.join(currentPage.split()).encode('utf-8')

		pageSelectorItems = pageSelector.xpath('div[contains(concat(" ", \
		@class, " ")," owl-item ")]/div[contains(concat(" ", @class, " "), \
		" nav-item ")]')
		pageIncrement = ""
		pageCode = ""
		pageRoundMD = ""
		nextPage = ""
		lastPage = None
		# Para cada Item no Seletor
		if len(self.pages2Visit) > 0:
			for item in pageSelectorItems:
				pageName = ' '.join(item.xpath('a/text()').extract_first().split())
				#print(pageName+":"+self.pages2Visit[0])

				if (pageName == self.pages2Visit[0]):
					#print (page + ":" + (' '.join(pageName.split())))
					pageCode = item.xpath('a/@data-match-day').extract_first()
					pageRoundMD = "/"
					pageIncrement = "#md/"
					if pageCode is None:
						if item.xpath('a/@data-md').extract_first():
							pageCode = item.xpath('a/@data-round').extract_first()
							pageIncrement = "#rd/"
							pageRoundMD += item.xpath('a/@data-md').extract_first()
							#print (self.start_urls[0] + pageIncrement + pageCode + pageRoundMD)
						else:
							pageIncrement = "#rd/"
							pageCode = item.xpath('a/@data-round').extract_first()
							pageRoundMD = ""
					nextPage = self.start_urls[0] + pageIncrement + pageCode + pageRoundMD
					break



			if (currentPage == self.pages2Visit[0]):
				print("On page " + self.pages2Visit[0])


				rootInfo = response.xpath('//div[@class="matches-list"]')

				matchRows = rootInfo.xpath('div[contains(concat(" ", @class, " "), \
				" match-row ")]/a[@class="match-row_link"]')

				if len(matchRows) == 0 :
					#print ("))))))))))))))))))))))))))))))))))))")
					matchRows = rootInfo.xpath('div[contains(concat(" ", @class, " "), \
					" mm-wrap ")]/div[contains(concat(" ", @class, " "), \
					" mm-list ")]/div[contains(concat(" ", @class, " "), \
					" mm-match ")]/div[contains(concat(" ", @class, " "), \
					" match-row ")]/a[@class="match-row_link"]')


				for match in matchRows:
					teamHomeName = match.xpath('div[contains(concat(" ", @class, " "), \
					" match-row_match ")]/div[contains(concat(" ", @class, " "), \
					" team-home ")]/span[@class="team-name"]/text()').extract_first()
					teamHomeScore = match.xpath('div[contains(concat(" ", @class, " ") \
					, " match-row_match ")]/div[contains(concat(" ", @class, " "), \
					" match--score ")]/span[contains(concat(" ", @class, " "), \
					" match--score_score ")]/span[@class="js-team--home-score"]\
					/text()').extract_first()
					teamAwayScore = match.xpath('div[contains(concat(" ", @class, \
					" "), " match-row_match ")]/div[contains(concat(" ", @class, \
					" "), " match--score ")]/span[contains(concat(" ", @class, " "), \
					" match--score_score ")]/span[@class="js-team--away-score"]\
					/text()').extract_first()
					teamAwayName = match.xpath('div[contains(concat(" ", @class, \
					" "), " match-row_match ")]/div[contains(concat(" ", @class, " "),\
					 " team-away ")]/span[@class="team-name"]/text()').extract_first()
					matchLocationStadium = match.xpath('div[contains(concat(" ", \
					@class, " "), " match-row_info ")]/div[contains(concat(" ", \
					@class, " "), " match-row_info-wrap ")]/div[contains(concat(" ", \
					@class, " "), " match-location ")]/span[contains(concat(" ", \
					@class, " "), " match-location_stadium ")]/text()').extract_first()
					matchLocationVenue = match.xpath('div[contains(concat(" ", \
					@class, " "), " match-row_info ")]/div[contains(concat(" ", \
					@class, " "), " match-row_info-wrap ")]/div[contains(concat(" ", \
					@class, " "), " match-location ")]/span[contains(concat(" ", \
					@class, " "), " match-location_venue ")]/text()').extract_first()
					rawDATA = match.xpath('.//@data-options').extract_first() #["match"]["MatchDateTime"]
					jsonDATA = json.loads(rawDATA)
					#print(currentPage)
					if (currentPage.find("-")>0):
						currentPhase = currentPage.split(" - ")[0]
						phaseInfo = currentPage.split(" - ")[1]
					else:
						currentPhase = currentPage
						phaseInfo = ""
					formatDate = jsonDATA["match"]["MatchDateTime"][6:-2]
					matchDate = formatDate #datetime.datetime.fromtimestamp(float(formatDate)/1000).strftime('"%Y-%m-%d %H:%M:%S')

					#yield SplashRequest(absolute_next_url, callback=self.parse)
					yield{'homeTeamID': teamHomeName, 'homeScore': teamHomeScore, 'awayTeamID': \
					teamAwayName, 'awayScore': teamAwayScore, 'matchDate': matchDate, \
					'matchLocationStadium': matchLocationStadium, 'matchLocationVenue': \
					matchLocationVenue, 'phase': currentPhase, "phaseInfo" : phaseInfo}
				del self.pages2Visit[0]

			else:
				#print("Not on page " + self.pages2Visit[0])
				print("Currently on page: " + currentPage)

		if nextPage:
			print("Going to " + nextPage)
			yield SplashRequest(nextPage , self.parse,endpoint='execute', \
            cache_args=['lua_source'], args={'lua_source':script, 'wait': 5, \
			'images_enabled' : False},headers={'X-My-Header': 'value'}, \
			dont_filter=True)
