# -*- coding: utf-8 -*-
import scrapy
import json
import datetime
import requests
import base64
from scrapy_splash import SplashRequest

script = """
function main(splash)
	splash:go(splash.args.url)
    splash:wait(2)
    return {html=splash:html()}
end
"""

class championsLeagueTeamsSpider(scrapy.Spider):
	name = 'championsLeagueTeams'
	allowed_domains = ['http://kassiesa.net/uefa/clubs/']
	start_urls = ['http://kassiesa.net/uefa/clubs/index.html']
	custom_settings = {
		'ITEM_PIPELINES': {
			'bolaoScrap.pipelines.MongoDBPipelineTeams': 300
			}
		}

	def start_requests(self):
		for url in self.start_urls:
			yield SplashRequest(url, self.parse, args={'wait': 5, 'images_enabled' : False})

	def parse(self, response):

		teamTable = response.xpath('//div[@id="divMain"]/div[1]/div[1]/table[3]/tbody/tr[position()>1]')

		for row in teamTable:
			teamCountry = row.xpath('td[2]/a/text()').extract_first()
			if (teamCountry is None):
				teamCountry = row.xpath('td[2]/text()').extract_first()
			teamLogoURL = row.xpath('td[3]/a/@href').extract_first()
			teamName = row.xpath('td[3]/a/text()').extract_first()
			response = requests.get(self.allowed_domains[0] + teamLogoURL, stream=True)

			if response.status_code == 200:
				response.raw.decode_content = True
				logoBin = base64.b64encode(response.raw.read())
				teamLogoBin = logoBin
			else:
				teamLogoBin = None

				#teamLogoBin = base64.b64encode(image_file.read())


			yield{'name': teamName, 'country': teamCountry.split(' - ')[1], 'logo': teamLogoBin}
