var parser = require('rss-parser');

var reg = new RegExp(/<br\s*\/?>/g);
var leagueList = [];//{['name'='', 'location'= '','matches'=[{'team1Name': '', 'team1Score':'','team2Name': '', 'team2Score':'',}],]};

var league = "none";
var location;
var matchList = []; // [{'team1Name': '', 'team1Score':'','team2Name': '', 'team2Score':''},]

team1Name="";
team2Name="";
team1Score="";
team2Score="";
dashPos = -1;


parser.parseURL('http://www.soccerstats247.com/DailyMatchFeed.aspx?langId=12', function(err, parsed) {
  //console.log(parsed.feed.title);
  parsed.feed.entries.forEach(function(entry) {
    //console.log(entry.title + ':' + entry.link);
    //console.log(entry.content.split(reg));
    values = entry.content.split(reg);
    values.forEach(function(value){
        if (value.match('<b>')){ // First reading on league
            value = value.replace('<b>',"");
            value = value.replace('</b>',"");
            value = value.replace('</center>',"");
            value = value.replace('<center>',"");

            if (league == "none"){
                location = value.split(': ')[0]
                league = value.split(': ')[1];
            }
            else{ // All other readings on league
                leagueList.push('{ \
                    "name"= '+ league +'  \
                    "location"= '+ location +'  \
                    "matches"= '+ matchList +'  \
                }');

                // clear variables
                league = "none";
                location = "";
                matchList = [];
            }
        }
        else if((value.length<50)&&(value.length>9)){ // Reading on matches
            value = value.replace('<b>',"");
            value = value.replace('</b>',"");
            value = value.replace('</center>',"");
            value = value.replace('<center>',"");

            dashPos = value.indexOf('-')

            team1Name= value.slice(0,dashPos-3);
            team2Name= value.slice(dashPos+3,value.length);

            team1Score= value.slice(dashPos,dashPos-1);
            team2Score= value.slice(dashPos+1,dashPos+2);

            console.log(team1Name + " " + team1Score + " : " + team2Score + " " + team2Name);

            matchList.push('{ \
                "team1Name"= ' + team1Name + ',  \
                team1Score"= ' + team1Score + ',  \
                team2Name"= ' + team2Name + ',  \
                team2Score"= ' + team2Score + ',  \
                }');
        }
    })
    //console.log(Object.getOwnPropertyNames(entry));
  })
  //console.log(leagueList);
})
