var express = require('express')
,   mongoose = require('mongoose')
,   bodyParser = require('body-parser')
,   sassMiddleware    = require('node-sass-middleware')
,   path    = require('path')
,   pug = require('pug')
,   serveStatic = require('serve-static')
,   index = require('./routes/site/index')
,   api = require('./routes/api/index')
,   events = require('./routes/site/events')
,   pools = require('./routes/site/pools')
,   rules = require('./routes/site/rules')
,   oneEvent = require('./routes/site/oneEvent')
,   getResults = require('./routes/api/getResults')
,   morgan = require('morgan')
,   log4js = require("log4js");

mongoose.Promise = require('bluebird');

log4js.configure({
  appenders: [
      { type: 'stdout' },
      { type: 'file', filename: './log/app.log' }
  ]
})


var srcPath = __dirname + '/sass';
var destPath = __dirname + '/public/styles';

var app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());


var logger = log4js.getLogger();
var theHTTPLog = morgan("dev");


//if (app.get('env') == 'production') {
//  app.use(morgan('common', { skip: function(req, res) { return res.statusCode < 400 }, stream: __dirname + '/../morgan.log' }));
//} else {
  app.use(theHTTPLog);
//}




mongoose.connect('mongodb://localhost:27017/bolaoScrapTeste');

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

//Middleware to get request parameters
var loadMW = function(req, res, next) {
    res.params = req.params
    res.body = req.body
    next()
}

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
//Sass middleware
app.use('/styles', sassMiddleware({
  src: srcPath,
  dest: destPath,
  //prefix: '/public',
  debug: true,
  outputStyle: 'expanded' //compressed
}));

// The static middleware must come after the sass middleware
app.use('/',
  serveStatic('./public', {})
);

//Api endpoints
app.use('/api', api);
app.use('/api/getResults', getResults);

//Site endpoints
app.use('/', index);
app.use('/events', events);
app.use('/pools', pools);
app.use('/rules', loadMW , rules);
app.use('/event/:urlID?', loadMW , oneEvent);



app.listen(process.env.PORT || 4730);
console.log("Running on port: " + 4730);
module.exports = app;
